package com.example.mymetrimoniapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.example.mymetrimoniapp.activity.ActivityUserListByGender;
import com.example.mymetrimoniapp.activity.BaseActivity;
import com.example.mymetrimoniapp.activity.FavoriteActivity;
import com.example.mymetrimoniapp.activity.UserListActivity;
import com.example.mymetrimoniapp.activity.activity_user_search;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {


    @BindView(R.id.registration)
    CardView registration;
    @BindView(R.id.list)
    CardView list;
    @BindView(R.id.favorite)
    CardView favorite;
    @BindView(R.id.search)
    CardView search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
setUpActionBar(getString(R.string.lbl_dashboard),false);
    }



    @OnClick(R.id.registration)
    public void onRegistrationClicked() {
        Intent h = new Intent(getApplicationContext(), activity_Add_User.class);
        startActivity(h);

    }

    @OnClick(R.id.list)
    public void onListClicked() {
        Intent h = new Intent(getApplicationContext(), ActivityUserListByGender.class);
        startActivity(h);

    }

    @OnClick(R.id.favorite)
    public void onFavoriteClicked() {
        Intent h = new Intent(getApplicationContext(), FavoriteActivity.class);
        startActivity(h);
    }

    @OnClick(R.id.search)
    public void onSearchClicked() {
        Intent h = new Intent(getApplicationContext(), activity_user_search.class);
        startActivity(h);
    }
}