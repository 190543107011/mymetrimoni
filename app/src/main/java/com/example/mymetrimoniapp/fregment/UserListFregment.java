package com.example.mymetrimoniapp.fregment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.activity_Add_User;
import com.example.mymetrimoniapp.adapter.UserListAdapter;
import com.example.mymetrimoniapp.database.TblUser;
import com.example.mymetrimoniapp.model.UserModel;
import com.example.mymetrimoniapp.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserListFregment extends Fragment {

    @BindView(R.id.recuserlist)
    RecyclerView recuserlist;
    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.noDataFound)
    TextView noDataFound;


    public static UserListFregment getInstance(int gender) {
        UserListFregment fregment = new UserListFregment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.Gender, gender);
        fregment.setArguments(bundle);

        return fregment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fregment_user_list, null);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_user, menu);

    }

    void openAddUserScreen() {
        Intent intent = new Intent(getActivity(), activity_Add_User.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.ivadduser) {
openAddUserScreen();
        }
        return super.onOptionsItemSelected(item);
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to delete this user?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(getActivity()).deleteUserById(userList.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter() {
        userList.addAll(new TblUser(getActivity()).getUserListByGender(getArguments().getInt(Constant.Gender)));
        checkAndVisibleView();


    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            recuserlist.setLayoutManager(new GridLayoutManager(getActivity(), 1));
            adapter = new UserListAdapter(getActivity(), userList, new UserListAdapter.onDeleteClickListener() {
                @Override
                public void onClick(int position) {
                    showAlertDialog(position);
                }

                @Override
                public void onItemClick(int position) {
                    Intent intent = new Intent(getActivity(), activity_Add_User.class);
                    intent.putExtra(Constant.USER_OBJECT,userList.get(position));
                    startActivity(intent);
                }

                @Override
                public void onFavoriteClick(int position) {

                    new TblUser(getContext()).changeFavoriteById(userList.get(position).getUserId(),userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    adapter.notifyDataSetChanged();
                }
            }){
               ;
            };
            recuserlist.setAdapter(adapter);

            //Display Recycler View
            recuserlist.setVisibility(View.VISIBLE);
            noDataFound.setVisibility(View.GONE);
        } else {
            //Display No Data Found
            recuserlist.setVisibility(View.GONE);
            noDataFound.setVisibility(View.VISIBLE);
        }

    }

    @OnClick(R.id.btnadduser)
    public void onViewClicked() {
        openAddUserScreen();
    }
}