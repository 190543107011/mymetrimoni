package com.example.mymetrimoniapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.model.LanguageModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends BaseAdapter {

    Context context;
    ArrayList<LanguageModel> languageList;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languageList) {
        this.context = context;
        this.languageList = languageList;
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {

        View v = view;
        ViewHolder viewHolder;
        if (v == null){

        v =  LayoutInflater.from(context).inflate(R.layout.view_row_text, null);
        viewHolder = new ViewHolder(v);
        v.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) v.getTag();

        }

viewHolder.tvName.setText(languageList.get(i).getName());

        return v;
    }


    class ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
