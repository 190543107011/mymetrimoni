package com.example.mymetrimoniapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList;
    onDeleteClickListener onDeleteClick;


    public UserListAdapter(Context context, ArrayList<UserModel> userList, onDeleteClickListener onDeleteClick
    ) {
        this.context = context;
        this.userList = userList;
        this.onDeleteClick = onDeleteClick;

    }

    public interface onDeleteClickListener {
        void onClick(int position);

        void onItemClick(int position);

        void onFavoriteClick(int position);
    }

    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.txtfullname.setText(userList.get(position).getName() + "" + userList.get(position).getSurName());
        holder.txtlanguage.setText(userList.get(position).getLanguage() + " |" + userList.get(position).getCity());
        holder.txtnumber.setText(userList.get(position).getPhoneNumber());




        holder.ivdeleteuser.setOnClickListener(v -> {
            if (onDeleteClick != null) {
                onDeleteClick.onClick(position);
            }

        });

        holder.itemView.setOnClickListener(v -> {

            if (onDeleteClick != null) {
                onDeleteClick.onItemClick(position);
            }

        });

        if(userList.get(position).getIsFavorite()==0){
            holder.ivFavoriteUser.setImageResource(R.drawable.favorite);
        }else{
            holder.ivFavoriteUser.setImageResource(R.drawable.love);
        }

        holder.ivFavoriteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onDeleteClick != null){
                    onDeleteClick.onFavoriteClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtfullname)
        TextView txtfullname;
        @BindView(R.id.txtlanguage)
        TextView txtlanguage;
        @BindView(R.id.txtnumber)
        TextView txtnumber;
        @BindView(R.id.ivdeleteuser)
        ImageView ivdeleteuser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
