package com.example.mymetrimoniapp.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.mymetrimoniapp.fregment.UserListFregment;
import com.example.mymetrimoniapp.util.Constant;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {


    private String tabTitles[] = new String[]{"Male","Female"};
    private Context context;

    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior,Context context) {
        super(fm, behavior);
        this.context = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return UserListFregment.getInstance(Constant.MALE);
        }else{
            return UserListFregment.getInstance(Constant.FEMALE);
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
