package com.example.mymetrimoniapp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import com.example.mymetrimoniapp.activity.BaseActivity;
import com.example.mymetrimoniapp.adapter.CityAdapter;
import com.example.mymetrimoniapp.adapter.LanguageAdapter;
import com.example.mymetrimoniapp.database.TblMasterLanguage;
import com.example.mymetrimoniapp.database.TblMstCity;
import com.example.mymetrimoniapp.database.TblUser;
import com.example.mymetrimoniapp.fregment.UserListFregment;
import com.example.mymetrimoniapp.model.CityModel;
import com.example.mymetrimoniapp.model.LanguageModel;
import com.example.mymetrimoniapp.model.UserModel;
import com.example.mymetrimoniapp.util.Constant;
import com.example.mymetrimoniapp.util.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class activity_Add_User extends BaseActivity {

    @BindView(R.id.edtname)
    EditText edtname;
    @BindView(R.id.edtfathername)
    EditText edtfathername;
    @BindView(R.id.edtsurname)
    EditText edtsurname;
    @BindView(R.id.rbmale)
    RadioButton rbmale;
    @BindView(R.id.rbfemale)
    RadioButton rbfemale;
    @BindView(R.id.rggroup)
    RadioGroup rggroup;
    @BindView(R.id.edtdob)
    EditText edtdob;
    @BindView(R.id.edtemail)
    EditText edtemail;
    @BindView(R.id.edtphonenumber)
    EditText edtphonenumber;
    @BindView(R.id.spcity)
    Spinner spcity;
    @BindView(R.id.splanguage)
    Spinner splanguage;
    @BindView(R.id.chksinging)
    CheckBox chksinging;
    @BindView(R.id.chkdancing)
    CheckBox chkdancing;
    @BindView(R.id.chkcooking)
    CheckBox chkcooking;
    @BindView(R.id.btnsubmit)
    Button btnsubmit;
    @BindView(R.id.screenlayout)
    FrameLayout screenlayout;

    String startingDate = "1999-03-27";

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();

    UserModel userModel;

    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__add__user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_adduser), true);
        getDataForUpdate();
        setDataToView();

    }

    void getDataForUpdate(){
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.lbl_useredit);
            userId = userModel.getUserId();
            edtname.setText(userModel.getName());
            edtfathername.setText(userModel.getFatherName());
            edtsurname.setText(userModel.getSurName());
            edtemail.setText(userModel.getEmail());
            edtphonenumber.setText(userModel.getPhoneNumber());
            edtdob.setText(userModel.getDob());
            if (userModel.getGender() == Constant.MALE) {
                rbmale.setChecked(true);
            } else {
                rbfemale.setChecked(true);
            }
            spcity.setSelection(getSelectedPositionFromCityId(userModel.getCityId()));
            splanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getLanguageId()));

        }

    }
    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityId() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getLanguageId() == languageId) {
                return i;
            }
        }
        return 0;
    }

    void setSpinnerAdapter() {
        cityList.addAll(new TblMstCity(this).getCityList());
        cityAdapter = new CityAdapter(this, cityList);
        spcity.setAdapter(cityAdapter);

        languageList.addAll(new TblMasterLanguage(this).getLanguages());
        languageAdapter = new LanguageAdapter(this, languageList);
        splanguage.setAdapter(languageAdapter);



    }

    void setDataToView() {
        final Calendar newCalendar = Calendar.getInstance();
        edtdob.setText(String.format(startingDate));
        setSpinnerAdapter();

    }


    @OnClick(R.id.edtdob)
    public void onEdtdobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                activity_Add_User.this, (datePicker, year, month, day) ->
                edtdob.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year)

                , newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @OnClick(R.id.btnsubmit)
    public void onBtnsubmitClicked() {
        if (isvalid()) {

            if (userModel == null){

                UserModel userModel = new UserModel();

                userModel.setName(edtname.getText().toString());
                userModel.setSurName(edtsurname.getText().toString());
                userModel.setDob(edtdob.getText().toString());
                userModel.setGender(rbmale.isChecked() ? Constant.MALE : Constant.FEMALE);
                userModel.setLanguageId(languageList.get(splanguage.getSelectedItemPosition()).getLanguageId());
                userModel.setFatherName(edtfathername.getText().toString());
                userModel.setEmail(edtemail.getText().toString());
                userModel.setPhoneNumber(edtphonenumber.getText().toString());
                userModel.setCityId(cityList.get(spcity.getSelectedItemPosition()).getCityId());
                userModel.setHobbies(stringHobbies());
                userModel.setIsFavorite(0);

                long lastInsetedID = new TblUser(this).insertUser(userModel);

    /*long lastInsetedID = new  TblUser(getApplicationContext()).insertUser(edtfathername.getText().toString(),edtname.getText().toString(),
        edtsurname.getText().toString(),,utils.getFormatedDateToInsert(edtdob.getText().toString()),
        edtphonenumber.getText().toString(),languageList.get(splanguage.getSelectedItemPosition()).getLanguageId(),
        cityList.get(spcity.getSelectedItemPosition()).getCityId(),"",edtemail.getText().toString());*/
                showToast(lastInsetedID > 0 ? "User Inserted Successfully" : "Something went wrong");
                finish();

            }else {


                UserModel userModel = new UserModel();
                userModel.setUserId(userId);
                userModel.setName(edtname.getText().toString());
                userModel.setSurName(edtsurname.getText().toString());
                userModel.setDob(edtdob.getText().toString());
                userModel.setGender(rbmale.isChecked() ? Constant.MALE : Constant.FEMALE);
                userModel.setLanguageId(languageList.get(splanguage.getSelectedItemPosition()).getLanguageId());
                userModel.setFatherName(edtfathername.getText().toString());
                userModel.setEmail(edtemail.getText().toString());
                userModel.setPhoneNumber(edtphonenumber.getText().toString());
                userModel.setCityId(cityList.get(spcity.getSelectedItemPosition()).getCityId());
                userModel.setHobbies(stringHobbies());

                long lastInsetedID = new TblUser(this).updateUserById(userModel);

    /*long lastInsetedID = new  TblUser(getApplicationContext()).insertUser(edtfathername.getText().toString(),edtname.getText().toString(),
        edtsurname.getText().toString(),,utils.getFormatedDateToInsert(edtdob.getText().toString()),
        edtphonenumber.getText().toString(),languageList.get(splanguage.getSelectedItemPosition()).getLanguageId(),
        cityList.get(spcity.getSelectedItemPosition()).getCityId(),"",edtemail.getText().toString());*/
                showToast(lastInsetedID > 0 ? "User Update Successfully" : "Something went wrong");

                finish();
            }

        }

    }

    public Boolean isvalid() {
        Boolean booleanisvalide = true;

        String email = edtemail.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String MobilePattern = "[0-9]{10}";


        if (edtname.getText().toString().isEmpty()) {
            booleanisvalide = false;
            edtname.setError("Enter First Name");

        }
        if (edtfathername.getText().toString().isEmpty()) {
            booleanisvalide = false;
            edtfathername.setError("Enter Father Name");

        }
        if (edtsurname.getText().toString().isEmpty()) {
            booleanisvalide = false;
            edtsurname.setError("Enter  SurName");
        }



        if (!email.matches(emailPattern)) {
            booleanisvalide = false;
            edtemail.setError("Enter valid Email");

        }
        if (!edtphonenumber.getText().toString().matches(MobilePattern)) {
            booleanisvalide = false;
            edtphonenumber.setError("Enter Phone Number");

        }
        if (spcity.getSelectedItemPosition() == 0) {
            booleanisvalide = false;
            Toast.makeText(getApplicationContext(), "plese choose any city", Toast.LENGTH_SHORT).show();
        }

        if (splanguage.getSelectedItemPosition() == 0) {
            booleanisvalide = false;
            Toast.makeText(getApplicationContext(), "plese choose any Language", Toast.LENGTH_SHORT).show();
        }

        if (!(chksinging.isChecked() || chkdancing.isChecked() || chkcooking.isChecked())) {
            booleanisvalide = false;
            Toast.makeText(getApplicationContext(), "plese choose any one", Toast.LENGTH_SHORT).show();
        }
        if (!(rbmale.isChecked() || rbfemale.isChecked())){
            Toast.makeText(getApplicationContext(),"plese choose Gender",Toast.LENGTH_SHORT).show();
            return booleanisvalide;
        }
        return booleanisvalide;
    }

    String stringHobbies() {
        String tempHobbies = "";

        if (chkdancing.isChecked()) {
            tempHobbies += "Dancing ";
        }

        if (chksinging.isChecked()) {
            tempHobbies += "Singing ";
        }

        if (chkcooking.isChecked()) {
            tempHobbies += "Cooking";
        }


        return tempHobbies;
    }

}