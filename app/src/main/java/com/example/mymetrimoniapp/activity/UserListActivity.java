package com.example.mymetrimoniapp.activity;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.adapter.UserListAdapter;
import com.example.mymetrimoniapp.database.TblUser;
import com.example.mymetrimoniapp.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListActivity extends BaseActivity {
    @BindView(R.id.recuserlist)
    RecyclerView recuserlist;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_userlist), true);
        setAdapter();
    }
        void setAdapter(){
            recuserlist.setLayoutManager(new GridLayoutManager(this,1));
            userList.addAll(new TblUser(this).getUserList());
            adapter = new UserListAdapter(this,userList,null);
            recuserlist.setAdapter(adapter);
    }
}
