package com.example.mymetrimoniapp.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.adapter.UserListAdapter;
import com.example.mymetrimoniapp.database.TblUser;
import com.example.mymetrimoniapp.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class activity_user_search extends BaseActivity {
    @BindView(R.id.edtusersearch)
    EditText edtusersearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;

    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempuserList = new ArrayList<>();
    UserListAdapter adapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_user_search), true);
        setAdapter();
        setsearch();
    }
    void resetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    void  setsearch(){
       edtusersearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tempuserList.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < userList.size(); j++) {
                        if (userList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getSurName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            tempuserList.add(userList.get(j));
                        }
                    }
                }
                if (charSequence.toString().length() == 0 && tempuserList.size() == 0) {
                    tempuserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void setAdapter(){
        rcvUsers.setLayoutManager(new GridLayoutManager(this,1));
        userList.addAll(new TblUser(this).getUserList());
        tempuserList.addAll(userList);
        adapter = new UserListAdapter(this,tempuserList,null);
        rcvUsers.setAdapter(adapter);
    }
}
