package com.example.mymetrimoniapp.activity;

import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class ActivityUserListByGender extends BaseActivity {

    @BindView(R.id.tlgenders)
    TabLayout tlgenders;
    @BindView(R.id.vpuserlist)
    ViewPager vpuserlist;
   ViewPagerAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender_wise_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_userlist), true);
        setUpViewPagerAdapter();
    }
    void  setUpViewPagerAdapter(){
    adapter = new ViewPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,this);
    vpuserlist.setAdapter(adapter);
    tlgenders.setupWithViewPager(vpuserlist);
    }
}
