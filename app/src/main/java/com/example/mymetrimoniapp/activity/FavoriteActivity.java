package com.example.mymetrimoniapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymetrimoniapp.R;
import com.example.mymetrimoniapp.activity_Add_User;
import com.example.mymetrimoniapp.adapter.UserListAdapter;
import com.example.mymetrimoniapp.database.TblUser;
import com.example.mymetrimoniapp.model.UserModel;
import com.example.mymetrimoniapp.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteActivity extends BaseActivity {
    @BindView(R.id.recuserlist)
    RecyclerView recuserlist;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar("Favorite List", true);
        setAdapter();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            recuserlist.setLayoutManager(new GridLayoutManager(FavoriteActivity.this, 1));
            adapter = new UserListAdapter(FavoriteActivity.this, userList, new UserListAdapter.onDeleteClickListener() {
                @Override
                public void onClick(int position) {
                    showAlertDialog(position);
                }

                @Override
                public void onItemClick(int position) {
                    Intent intent = new Intent(FavoriteActivity.this, activity_Add_User.class);
                    intent.putExtra(Constant.USER_OBJECT,userList.get(position));
                    startActivity(intent);
                }

                @Override
                public void onFavoriteClick(int position) {

                    new TblUser(FavoriteActivity.this).changeFavoriteById(userList.get(position).getUserId(),userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    adapter.notifyDataSetChanged();
                }
            }){
                ;
            };
            recuserlist.setAdapter(adapter);

            //Display Recycler View
            recuserlist.setVisibility(View.VISIBLE);
            //noDataFound.setVisibility(View.GONE);
        } else {
            //Display No Data Found
            recuserlist.setVisibility(View.GONE);
            //noDataFound.setVisibility(View.VISIBLE);
        }

    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(FavoriteActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to delete this user?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(FavoriteActivity.this).deleteUserById(userList.get(position).getUserId());
                    if (deletedUserId > 0) {
                        Toast.makeText(FavoriteActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(FavoriteActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter(){
        recuserlist.setLayoutManager(new GridLayoutManager(this,1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.onDeleteClickListener() {
            @Override
            public void onClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(FavoriteActivity.this, activity_Add_User.class);
                intent.putExtra(Constant.USER_OBJECT,userList.get(position));
                startActivity(intent);
            }

            @Override
            public void onFavoriteClick(int position) {

                new TblUser(FavoriteActivity.this).changeFavoriteById(userList.get(position).getUserId(),userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                adapter.notifyDataSetChanged();
            }
        }){
            ;
        };
        recuserlist.setAdapter(adapter);
    }
}
