package com.example.mymetrimoniapp.util;

public class Constant {

    public static final int MALE = 1;
    public static final int FEMALE = 2;
    public static final int OTHER = 3;

    public static final String Gender = "Gender";

    public static final String USER_OBJECT = "UserObject";
}
