package com.example.mymetrimoniapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mymetrimoniapp.model.CityModel;
import com.example.mymetrimoniapp.model.UserModel;
import com.example.mymetrimoniapp.util.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TblUser extends Mydatabase {

    public static final String TABLE_NAME = " TblUser";
    public static final String User_Id = "UserId";
    public static final String Name = "Name";
    public static final String Father_Name = "FatherName";
    public static final String Sur_Name = "SurName";
    public static final String Gender = "Gender";
    public static final String Hobbies = "Hobbies";
    public static final String Dob = "Dob";
    public static final String Phone_Number = "PhoneNumber";
    public static final String Language_Id = "LanguageId";
    public static final String City_Id = "CityId";
    public static final String Language = "Language";
    public static final String City = "City";
    public static final String Age= "Age";
    public static final String Email = "Email";
    public static final String IsFavorite = "IsFavorite";



    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                "SELECT " +
                        " UserId," +
                        " TblUser.Name as Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Email," +
                        " Dob," +
                        " PhoneNumber," +
                        " IsFavorite, "+
                        " TblMstLanguage.LanguageID," +
                        " TblMstCity.CityID," +
                        " TblMstLanguage.Name as Language," +
                        " TblMstCity.Name as City " +
                        "FROM " +
                        " TblUser " +
                        " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                        " INNER JOIN TblMstCity ON TblUser.CityID = TblMstCity.CityID";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return list;
    }

    public ArrayList<UserModel> getFavoriteUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                "SELECT " +
                        " UserId," +
                        " TblUser.Name as Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Email," +
                        " Dob," +
                        " PhoneNumber," +
                        " IsFavorite, "+
                        " TblMstLanguage.LanguageID," +
                        " TblMstCity.CityID," +
                        " TblMstLanguage.Name as Language," +
                        " TblMstCity.Name as City " +
                        "FROM " +
                        " TblUser " +
                        " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                        " INNER JOIN TblMstCity ON TblUser.CityID = TblMstCity.CityID"+
                " Where "+IsFavorite+ " = "+1 ;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getCreateModelUsingCursor(Cursor cursor){
        UserModel model = new UserModel();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(User_Id)));
        model.setName(cursor.getString(cursor.getColumnIndex(Name)));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(Father_Name)));
        model.setSurName(cursor.getString(cursor.getColumnIndex(Sur_Name)));
        model.setGender(cursor.getInt(cursor.getColumnIndex(Gender)));
       model.setDob(cursor.getString(cursor.getColumnIndex(Dob)));

  //      model.setHobbies(cursor.getString(cursor.getColumnIndex(Hobbies)));
        model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(Phone_Number)));
        model.setLanguageId(cursor.getInt(cursor.getColumnIndex(Language_Id)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(City_Id)));
        model.setCity(cursor.getString(cursor.getColumnIndex(City)));
        model.setLanguage(cursor.getString(cursor.getColumnIndex(Language)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(Email)));
        model.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IsFavorite)));

        return model;

    }
    private String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.set(year, month, day);
        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();
        return ageS;
    }

    public UserModel getUserById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        UserModel model = new UserModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + City_Id + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
      model = getCreateModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return model;

    }
    public ArrayList<UserModel>getUserListByGender(int gender){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                "SELECT " +
                        " UserId," +
                        " TblUser.Name as Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Email," +
                        " Dob," +
                        " PhoneNumber," +
                        " IsFavorite, "+
                        " TblMstLanguage.LanguageID," +
                        " TblMstCity.CityID," +
                        " TblMstLanguage.Name as Language," +
                        " TblMstCity.Name as City " +

                        "FROM " +
                        " TblUser " +
                        " INNER JOIN TblMstLanguage ON TblUser.LanguageID = TblMstLanguage.LanguageID" +
                        " INNER JOIN TblMstCity ON TblUser.CityID = TblMstCity.CityID " +
                "WHERE " + Gender + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreateModelUsingCursor(cursor));
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return list;

    }


    public long insertUser(UserModel userModel){

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = returnSetContentValue(userModel);
        long lastInsertId= db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastInsertId;

    }

    ContentValues returnSetContentValue(UserModel userModel){
        ContentValues cv = new ContentValues();
        cv.put(Name,userModel.getName());
        cv.put(Father_Name,userModel.getFatherName());
        cv.put(Sur_Name,userModel.getSurName());
        cv.put(Gender,userModel.getGender());
        cv.put(Dob, userModel.getDob());
        cv.put(Phone_Number,userModel.getPhoneNumber());
        cv.put(Language_Id,userModel.getLanguageId());
        cv.put(City_Id,userModel.getCityId());
        cv.put(Hobbies,userModel.getHobbies());
        cv.put(Email,userModel.getEmail());
        cv.put(IsFavorite,userModel.getIsFavorite());

        return cv;
    }


    public long updateUserById(UserModel userModel){

        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = returnSetContentValue(userModel);
        int lastUpdatedId= db.update(TABLE_NAME,cv,User_Id+" = ?",new  String[]{String.valueOf(userModel.getUserId())});
        db.close();
        return lastUpdatedId;

    }
    public int deleteUserById(int userId){
        SQLiteDatabase db = getWritableDatabase();
      int lastDeleteId=  db.delete(TABLE_NAME,User_Id+" = ?",new  String[]{String.valueOf(userId)});
        db.close();
        return lastDeleteId;
    }

    public void changeFavoriteById(int userId,int isfavorite){
        SQLiteDatabase db = getWritableDatabase();
        String query = "update "+TABLE_NAME + " Set "+IsFavorite+" = "+isfavorite+" where "+User_Id+" = "+userId;
        db.execSQL(query);
        //int favorite=  db.delete(TABLE_NAME,IsFavorite+" = ?",new  String[]{String.valueOf(userId)});
        db.close();
    }

}
