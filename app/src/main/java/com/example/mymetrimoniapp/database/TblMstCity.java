package com.example.mymetrimoniapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mymetrimoniapp.model.CityModel;

import java.util.ArrayList;
import java.util.HashMap;

public class TblMstCity extends Mydatabase {

    public static final String TABLE_NAME = "TblMstCity";
    public static final String City_Id = "CityId";
    public static final String Name= "Name";
    public TblMstCity(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCityList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList <CityModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor  = db.rawQuery(query,null);
        cursor.moveToFirst();
        CityModel cityModel1 = new CityModel();
        cityModel1.setName("Select One");
        list.add(0, cityModel1);
        for (int i = 0; i < cursor.getCount(); i++){
            CityModel cityModel = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(City_Id)));
            cityModel.setName(cursor.getString(cursor.getColumnIndex(Name)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public CityModel getCityById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        CityModel model = new CityModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + City_Id + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setName(cursor.getString(cursor.getColumnIndex(Name)));
        model.setCityId(cursor.getInt(cursor.getColumnIndex(City_Id)));
        cursor.close();
        db.close();
        return model;
    }
}
