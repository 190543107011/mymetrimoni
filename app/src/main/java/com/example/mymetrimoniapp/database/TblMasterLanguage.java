package com.example.mymetrimoniapp.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mymetrimoniapp.model.LanguageModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TblMasterLanguage extends Mydatabase {

    public static final String TABLE_NAME = "TblMstLanguage";
    public static final String LanguageId = "LanguageId";
    public static final String Name= "Name";

    public TblMasterLanguage(Context context) {
        super(context);
    }

    public ArrayList<LanguageModel> getLanguages(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT * FROM "+ TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        LanguageModel languageModel1 = new LanguageModel();
        languageModel1.setName("Select One");
        list.add(0, languageModel1);
        for (int i = 0;i<cursor.getCount();i++){
            LanguageModel languageModel = new LanguageModel();
            languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LanguageId)));
            languageModel.setName(cursor.getString(cursor.getColumnIndex(Name)));
            list.add(languageModel);
            cursor.moveToNext();

        }
        cursor.close();
        db.close();
        return list;
    }
}
